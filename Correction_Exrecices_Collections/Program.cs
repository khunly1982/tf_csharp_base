﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Correction_Exrecices_Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Exercice 1
            //Console.WriteLine("Entrer un nombre");
            //int nb = int.Parse(Console.ReadLine());
            //List<int> premiers = new List<int>();

            //int nbToTest = 2;

            //while(nbToTest < nb)
            //{
            //    bool estPremier = true;
            //    // tester si nbToToTest est premier
            //    for (int i = 2; i <= nbToTest / 2 && estPremier; i++)
            //    {
            //        if(nbToTest % i == 0)
            //        {
            //            // le nombre n'est pas premier
            //            estPremier = false;
            //        }
            //    }
            //    // si il est premier 
            //    if (estPremier)
            //        premiers.Add(nbToTest);
            //    // ajouter ce nombre dans la liste
            //    nbToTest++;
            //}

            // afficher la liste
            // Console.WriteLine(string.Join(", ", premiers));
            #endregion

            #region Exercice 2
            //Console.WriteLine("Entrer un nombre");
            //int nb = int.Parse(Console.ReadLine());
            //List<int> premiers = new List<int>();

            //int nbToTest = 2;

            //while (nbToTest < nb)
            //{
            //    bool estPremier = true;
            //    // tester si nbToToTest est premier
            //    foreach(int i in premiers/*.Where(x => estPremier)*/)
            //    {
            //        if (nbToTest % i == 0)
            //        {
            //            // le nombre n'est pas premier
            //            estPremier = false;
            //        }
            //    }
            //    // si il est premier 
            //    if (estPremier)
            //        premiers.Add(nbToTest);
            //    // ajouter ce nombre dans la liste
            //    nbToTest++;
            //}

            //// afficher la liste
            //Console.WriteLine(string.Join(", ", premiers));
            #endregion


            #region Exercice 3
            Console.WriteLine("Entrer 1 nombre");
            char[] nb1 = Console.ReadLine().ToCharArray();

            Console.WriteLine("Entrer 1 second nombre");
            char[] nb2 = Console.ReadLine().ToCharArray();

            double resultat = 0;

            for (int i = 0; i < nb1.Length; i++)
            {
                int v = int.Parse(nb1[i].ToString());
                resultat += v * Math.Pow(10, nb1.Length - i - 1);
            }
            for (int i = 0; i < nb2.Length; i++)
            {
                int v = int.Parse(nb2[i].ToString());
                resultat += v * Math.Pow(10, nb2.Length - i - 1);
            }
            Console.WriteLine(resultat);
            #endregion
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_Structures
{
    struct Eleve
    {
        public string Nom;
        public DateTime DateDeNaissance;
        public Prof Prof;
    }
}

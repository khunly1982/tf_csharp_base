﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_Structures
{
    struct Prof
    {
        public string Nom;

        public int Age;

        public bool EstNomme;

        public List<Eleve> Eleves;

    }
}

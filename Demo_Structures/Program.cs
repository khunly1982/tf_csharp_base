﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Demo_Structures
{
    
    class Program
    {
        static void Main(string[] args)
        {
            string nom = "Khun";

            string[] noms = { "Khun", "Thierry" };

            int age = 39;

            ArrayList l = new ArrayList { "Khun", age };

            int ageDans2Ans = (int)l[1] + 2;

            Console.WriteLine(ageDans2Ans);

            Console.WriteLine(string.Join(",", l.ToArray()));

            (string, int)prof = ("Khun", 39);

            Console.WriteLine(prof.Item2 + 2);

            //Prof p = new Prof() { Nom = "Khun", Age = 39, EstNomme = false };
            //Prof p2 = new Prof() { Nom = "Mike", Age = 39, EstNomme = true };
            //Prof p3 = p2;

            //Console.WriteLine(p2.Age);
            //p2.Age = 50;
            //Console.WriteLine($"Le prof {p2.Nom} a maintenant {p2.Age} ans");
            //Console.WriteLine($"Le prof {p3.Nom} a maintenant {p3.Age} ans");


            //List<Prof> profs = new List<Prof>
            //{
            //    new Prof() { Nom = "Khun", Age = 39, EstNomme = false },
            //    new Prof() { Nom = "Mike", Age = 39, EstNomme = true },
            //};
            //Console.WriteLine($"Le prof {profs[0].Nom} a maintenant {profs[0].Age} ans");

            
        }
    }
}

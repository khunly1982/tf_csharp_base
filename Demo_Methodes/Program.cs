﻿using System;
using System.Linq;

namespace Demo_Methodes
{
    class Program
    {
        static void Main(string[] args)
        {
            //string s = null;
            //Console.WriteLine(s?.Length ?? (object)"Pas de valeur");
            //int nb1 = AskNumber("Veuillez entrer un premier nombre");
            //int nb2 = AskNumber("Veuillez entrer un second nombre");

            //Console.WriteLine($"La somme des 2 nombres est égale à {nb1 + nb2}");

            //int s = Somme(7, 3, 77, 42);
            //Console.WriteLine(s);

            WriteMessage(out string message);
            Console.WriteLine(message);
        }

        /// <summary>
        /// Demande un nombre et vérifie s'il s'agit d'un nombre correcte
        /// </summary>
        /// <param name="message">Le message qui sera écrit au départ</param>
        /// <param name="messageErreur">Le message affiché en cas d'erreur</param>
        /// <returns>le nombre entré par l'utilisateur</returns>
        static int AskNumber(string message, string messageErreur = "Valeur incorrecte")
        {
            Console.WriteLine(message);
            string entree = Console.ReadLine();
            int nb;
            while (!int.TryParse(entree, out nb))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(messageErreur);
                Console.ResetColor();
                entree = Console.ReadLine();
                Console.Clear();
            }
            return nb;
        }

        static int Somme(int mutiplicateur, params int[] nombres)
        {
            int result = 0;
            foreach (int nb in nombres)
            {
                result += nb;
            }
            return result * mutiplicateur;
        }

        static void WriteMessage(out string message)
        {
            message = "Hello";
            Console.WriteLine(message);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorrectionStructures
{
    struct Farenheit
    {
        public double Temperature;

        public Celcius ConvertirEnCelcius()
        {
            double value = (Temperature - 32) * 5 / 9;
            return new Celcius { Temperature = value };
        }
    }
}

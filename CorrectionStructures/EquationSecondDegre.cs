﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorrectionStructures
{
    struct EquationSecondDegre
    {
        // Ax² + Bx + C = 0
        public double A;
        public double B;
        public double C;

        public bool ResoudreEquation(out double? x1, out double? x2)
        {
            double delta = B * B - 4 * A * C;
            if(delta < 0)
            {
                x1 = null;
                x2 = null;
                return false;
            }
            // instructions ne seront executées que si on rentre pas dans le if
            x1 = (-1 * B + Math.Sqrt(delta)) / (2 * A);
            x2 = (-1 * B - Math.Sqrt(delta)) / (2 * A);
            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorrectionStructures
{
    struct Celcius
    {
        public double Temperature;

        public Farenheit ConvertirEnFarenheit()
        {
            double value = Temperature * 9 / 5 + 32;
            return new Farenheit { Temperature = value };
        }
    }
}

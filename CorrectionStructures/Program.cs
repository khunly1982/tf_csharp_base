﻿using System;

namespace CorrectionStructures
{
    class Program
    {
        static void Main(string[] args)
        {
            // type[,] nomDeLaVariable = new type[nElements,nb]
            //Point?[,] tableau = new Point?[5, 5];
            //for(int i=0; i<tableau.GetLength(0); i++)
            //{
            //    tableau[i, i] = new Point { X = i+1, Y = i+1 };
            //}

            //for (int j = 0; j < tableau.GetLength(1); j++)
            //{
            //    for (int i = 0; i < tableau.GetLength(0); i++)
            //    {
            //        Point? p = tableau[i, j];
            //        if (p is null)
            //        {
            //            Console.Write("       ");
            //        }
            //        else
            //        {
            //            Console.Write($"X:{p?.X}-Y:{p?.Y}");
            //        }
            //    }
            //    Console.Write("\n");
            //}

            //Celcius c = new Celcius { Temperature = 30 };
            //Farenheit f = c.ConvertirEnFarenheit();
            //Console.WriteLine($"Temp en farenheit = {f.Temperature}°F");

            //Farenheit f1 = new Farenheit { Temperature = 94 };
            //Celcius c1 = f1.ConvertirEnCelcius();
            //Console.WriteLine($"Temp en Celcius = {c1.Temperature}°C");

            EquationSecondDegre eq = new EquationSecondDegre();

            eq.A = double.Parse(Console.ReadLine());
            eq.B = double.Parse(Console.ReadLine());
            eq.C = double.Parse(Console.ReadLine());

            if(eq.ResoudreEquation(out double? x1, out double? x2))
            {
                Console.WriteLine($"Les solutions de l'équation sont {x1}, {x2}");
            }
            else 
            {
                Console.WriteLine("Pas de solution");
            }

        }
    }
}

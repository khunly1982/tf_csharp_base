﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Demo_Boucles
{
    class Program
    {
        static void Main(string[] args)
        {
            // on utilisera la boucle for lorsque
            // on sait le nombre de fois que l'on souhaite itérer
            //for (int i = 0; i <= 15; i++)
            //{
            //    Console.WriteLine(i * i);
            //}

            // while est utilisé quand on ne connait le nombre d'iterations
            //Console.WriteLine("Veuillez entrer un nombre");
            //string entree = Console.ReadLine();

            //int nb;
            //while(!int.TryParse(entree, out nb))
            //{
            //    Console.WriteLine("le nombre est incorrect");
            //    entree = Console.ReadLine();
            //}
            //Console.WriteLine($"Le nombre entre est {nb * 100}");

            //int nb;
            //string entree;
            //do
            //{
            //    // instructions que l'on veut repeter
            //    Console.WriteLine("Entrer un nombre");
            //    entree = Console.ReadLine();
            //} while (!int.TryParse(entree, out nb));

            string[] tab = new string[] { "riri", "fifi", "loulou" };
            for (int i = 0; i < tab.Length; i++)
            {
                Console.WriteLine(tab[i]);
            }

            foreach (string item in tab)
            {
                //if(item == "fifi")
                //{
                //    //continue;
                //}
                Console.WriteLine(item);
            }
        }
    }
}

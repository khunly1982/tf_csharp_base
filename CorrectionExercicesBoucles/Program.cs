﻿using System;

namespace CorrectionExercicesBoucles
{
    class Program
    {
        static void Main(string[] args)
        {
            #region exercice 1
            //int f0 = 0;
            //Console.WriteLine($"f(0) = {f0}");
            //int f1 = 1;
            //Console.WriteLine($"f(1) = {f1}");
            //for (int i = 2; i < 25; i++)
            //{
            //} 
            //    int fn = f0 + f1;
            //    f0 = f1;
            //    f1 = fn;
            //    Console.WriteLine($"f({i}) = {fn}");
            #endregion

            #region exercice 2
            //Console.WriteLine("Entrer un nombre");
            //int nb = int.Parse(Console.ReadLine());

            //double res = 1; //1 * 2 * 3 * 4

            //for (int i = 2; i <= nb; i++)
            //{
            //    //res = res + 1;
            //    //res += 1;

            //    //res = res * i;
            //    res *= i;
            //}
            //Console.WriteLine($"{nb}! = {res}");
            #endregion

            #region exercice 3
            //int x = 2;
            //int nbTrouves = 0;
            //while (nbTrouves < 100)
            //{

            //    bool trouve = false;
            //    for (int i = 2; i <= x/2 && !trouve; i++)
            //    {
            //        if (x % i == 0)
            //        {
            //            //Console.WriteLine($"le nombre {x} n'est pas premier");
            //            trouve = true;
            //        }
            //    }
            //    if (!trouve)
            //    {
            //        Console.WriteLine($"Le nombre est premier : {x}");
            //        nbTrouves++;
            //    }
            //    x++;
            //}


            #endregion

            #region exercice 4
            //for (int table = 1; table <= 5; table++)
            //{
            //    for (int multi = 1; multi <= 20; multi++)
            //    {
            //        Console.WriteLine($"{table} X {multi} = {table * multi}");
            //    }
            //}

            #endregion

            #region exercice 5
            //for (decimal i = 0.0m; i < 20.0m; i += 0.1m)
            //{
            //    Console.WriteLine(i);
            //}
            #endregion

            #region exercice 6 Bonus
            // demander le nombre dont on souhaite connaitre la racine
            //Console.WriteLine("Entrer un nombre");
            //int b = int.Parse(Console.ReadLine());
            ////test
            //// on sait que x² - b = 0 lorsque x = (racine de b) ou (- racine de b)
            //// posons a une approximation (positive) de racine de b
            //// sur le graphique f(x) = x² - b on a le point(a, a²-b)
            //// calculons l'eq de la tangante en ce point
            //// on sait que sa pente nous est donné par f'(x) = 2x => f'(a) = 2a
            //// l'equation de la tangente est donc => t := y = 2a(x-a) + a²-b
            //// cette equation s'annule qd x = (a² + b) / (2a)
            //// on obtient alors un x qui est une meilleure approximation que ne l'était a
            //double a = 42; // nombre positif totalement arbitraire
            //// tant que le carré de l'approximation n'est pas suffisamment proche de b
            //while(Math.Abs(a * a - b) > 10e-9)
            //{
            //    a = (a * a + b) / (2 * a);
            //}
            //Console.WriteLine($"la racine carée de {b} = {a}");

            #endregion

            //string.Join(",", tab);

        }
    }
}

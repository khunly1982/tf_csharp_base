﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Demo_Tableau
{
    class Program
    {
        static void Main(string[] args)
        {
            // afficher un tableau 
            // Console.WriteLine(string.Join(',',tab));

            //int[] monTableau = new int[10];
            //monTableau[2] = 4;
            //monTableau[6] = 42;
            //int[] monTableau2 = monTableau;

            //// le tableau 1 sera aussi modifié
            //monTableau2[6] = 45;


            //int[,] matrice = new int[3, 5];

            //int nb = matrice[1, 2];


            //ArrayList list = new ArrayList();
            //list.Add(true);
            //list.Add(42);
            //list.Add("Hello");

            //List<int> l = new List<int>();
            //l.Add(42);
            //l.Add(0);
            //l.Add(42);
            //l.Add(42);

            //int nb = l.Count;

            // int value = l[0];


            // vider une liste
            // l.Clear();
            // retirer un element d'une liste
            // l.Remove(42);
            // retirer l'élément à la position 2
            // l.RemoveAt(2);

            // retirer tous les 42
            // l.RemoveAll(x => x == 42);
            // verifie si la valeur 42 est contenue dans la liste
            // bool exist = l.Contains(42);
            //foreach (int item in l)
            //{
            //    l[0] = 41;
            //}

            // dictionnaire

            //Dictionary<string, int> dico = new Dictionary<string, int>();

            //dico.Add("un", 1);
            // on ne peut pas donner 2 valeurs differentes à une meme clé
            // dico.Add("un", 2);
            //dico.Add("deux", 2);
            //dico.Add("trois", 3);

            // par contre on peut remplacer 
            // dico["un"] = 11;

            //Console.WriteLine(dico["deux"]); //2

            //foreach (int v in dico.Values)
            //{
            //    Console.WriteLine(v); // 1,2,3
            //}

            //foreach(string key in dico.Keys)
            //{
            //    Console.WriteLine(key); // un, deux, trois
            //}

            //foreach(KeyValuePair<string, int> kvp in dico)
            //{
            //    Console.WriteLine(kvp.Key);
            //    Console.WriteLine(kvp.Value);
            //}

            //ObservableCollection<string> collection = new ObservableCollection<string>();

            //collection.CollectionChanged += (sender, e) => { 
            //    Console.WriteLine(string.Join(", ", collection)); 
            //};

            //collection.Add("Thierry");
            //collection.Add("Khun");
            //collection.Add("Mike");

            //collection.Remove("Khun");


           


        }
    }
}
